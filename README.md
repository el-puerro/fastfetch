# fastfetch - a quick system information utility

![Screenshot of fastfetch](screen001.jpeg)

A quick system information utility, written in Rust (learning project),
inspired by pfetch.
