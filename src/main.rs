mod utils;
use colored::Colorize;

fn main() {
    println!("{}", utils::get_user_hostname().trim().underline());
    println!("os:     {}", utils::get_os().green());
    println!("host:   {}", utils::get_host_machine().green());
    println!("kernel: {}", utils::get_kernel_version().trim().green());
    println!("uptime: {}", utils::get_uptime().green());
    println!("pkgs:   {}", utils::get_pkgs_installed().green());
    println!("mem:    {}", utils::get_mem().green());
}
