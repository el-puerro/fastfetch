use std::fs;
use std::env;
use std::process::{Command, Stdio};
use std::path::PathBuf;
use std::io::{Read, Write};
use which::which;

pub fn get_os() -> String {
    let os_release = fs::read_to_string("/etc/os-release")
        .expect("/etc/os-release doesn't exist!");

    let mut os_info = os_release.split("\n");
    let os = os_info.next().unwrap().to_string()
        .replace(r#"NAME=""#, "").replace(r#"""#, "");

    return os;
}

pub fn get_host_machine() -> String {
    let n = fs::read_to_string("/sys/devices/virtual/dmi/id/product_name");
    let v = fs::read_to_string("/sys/devices/virtual/dmi/id/product_version");
    let m = fs::read_to_string("/sys/firmware/devicetree/base/model");
    
    let mut name = String::new();
    let mut version = String::new();
    let mut model = String::new();

    match n {
        Ok(x) => name = x,
        Err(_x) => name = "".to_string(),
    };
    match v {
        Ok(x) => version = x,
        Err(_x) => version = "".to_string(),
    };

    match m {
        Ok(x) => model = x,
        Err(_x) => model = "".to_string(),
    };

    return format!("{} {} {}", name.trim(), version.trim(), model.trim());
}

pub fn get_kernel_version() -> String {
    let uname = Command::new("uname").arg("-r").output()
        .expect("somehow there's no kernel version??!");

    return String::from_utf8(uname.stdout).unwrap();
}

pub fn get_uptime() -> String {
    let uptime = Command::new("uptime").arg("-p").output()
        .expect("no uptime info available!");

    let uptime_san = String::from_utf8(uptime.stdout).unwrap().replace("up ", "");
    return String::from(uptime_san.trim());
}

fn dnf_count_installed() -> String {
    let mut dnf = Command::new("dnf")
        .args(["list", "installed"])
        .stdout(Stdio::piped())
        .spawn()
        .unwrap();

    let mut count = Command::new("wc")
        .arg("-l")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()
        .unwrap(); //.expect("Can't count dnf packages!");

    if let Some(ref mut stdout) = dnf.stdout {
        if let Some(ref mut stdin) = count.stdin {
            let mut buf: Vec<u8> = Vec::new();
            stdout.read_to_end(&mut buf).unwrap();
            stdin.write_all(&buf).unwrap();
        }
    }

    let result = count.wait_with_output().unwrap().stdout;

    return String::from_utf8(result).unwrap().replace("\n", "") + " (dnf) ";

}

fn rpm_count_installed() -> String {
    let mut dnf = Command::new("rpm")
        .arg("-qa")
        .stdout(Stdio::piped())
        .spawn()
        .unwrap();

    let mut count = Command::new("wc")
        .arg("-l")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()
        .unwrap(); //.expect("Can't count dnf packages!");

    if let Some(ref mut stdout) = dnf.stdout {
        if let Some(ref mut stdin) = count.stdin {
            let mut buf: Vec<u8> = Vec::new();
            stdout.read_to_end(&mut buf).unwrap();
            stdin.write_all(&buf).unwrap();
        }
    }

    let result = count.wait_with_output().unwrap().stdout;

    return String::from_utf8(result).unwrap().replace("\n", "") + " (rpm) ";

}

pub fn get_pkgs_installed() -> String {
    let mut pkgcount = String::new();
    let package_managers = vec![
        "dnf",
        "bonsai",
        "pkginfo",
        "pacman",
        "dpkg",
        "rpm",
        "xbps-query",
        "apk",
        "guix",
        "opkg",
        "kiss",
        "cpt-list",
        "brew",
        "emerge",
        "pkgtool",
        "eopkg"];

    let pkgout: Vec<_> = package_managers
        .iter()
        .map(|x| match which(x) {
            Ok(i) => i,
            Err(_i) => PathBuf::new(),
        })
        .collect();

        //println!("{:?}", pkgout);

    for element in pkgout {
        match element.into_os_string().to_str().unwrap() {
            //"/usr/bin/dnf" => pkgcount.push_str(dnf_count_installed().as_str()),
            "/usr/bin/rpm" => pkgcount.push_str(rpm_count_installed().as_str()),
            "" => pkgcount.push_str(""),
            &_ => pkgcount.push_str(""),
        };
    }

    return pkgcount;
}
pub fn get_user_hostname() -> String {
    let hostname = fs::read_to_string("/etc/hostname").unwrap();
    let username = env::var("USER").unwrap();

    return username + "@" + hostname.as_str();

}

pub fn get_mem() -> String {
    let meminfo = fs::read_to_string("/proc/meminfo").unwrap();

    // parsing /proc/meminfo

    let mut total: u32 = 0;
    let mut memfree: u32= 0;
    let mut bufs: u32 = 0;
    let mut cached: u32 = 0;
    let mut shmem: u32 = 0;
    let mut sreclaim: u32 = 0;

    for x in meminfo.split("\n") {
        if x.contains("MemTotal") {
            total = x
            .replace("MemTotal:", "")
            .replace("kB", "")
            .trim()
            .parse::<u32>()
            .unwrap();
        }

        if x.contains("MemFree") {
            memfree = x
            .replace("MemFree:", "")
            .replace("kB", "")
            .trim()
            .parse::<u32>()
            .unwrap();
        }

        if x.contains("Buffers") {
            bufs = x
            .replace("Buffers:", "")
            .replace("kB", "")
            .trim()
            .parse::<u32>()
            .unwrap();
        }

        if x.contains("Cached") && !x.contains("SwapCached") {
            cached = x
            .replace("Cached:", "")
            .replace("kB", "")
            .trim()
            .parse::<u32>()
            .unwrap();
        }

        if x.contains("Shmem:") {
            shmem = x
            .replace("Shmem:", "")
            .replace("kB", "")
            .trim()
            .parse::<u32>()
            .unwrap();
        }

        if x.contains("Sreclaimable") {
            sreclaim = x
            .replace("Sreclaimable:", "")
            .replace("kB", "")
            .trim()
            .parse::<u32>()
            .unwrap();
        }
    }

    // calculating actually used memory

    let mut used = (total + shmem) - memfree - bufs - cached - sreclaim;

    used = used / 1000;
    total = total / 1000;

    return format!("{}M / {}M", used, total);
}

